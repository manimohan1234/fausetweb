import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from "@angular/common/http";
@Injectable({
  providedIn: 'root'
})
export class CrudService {


  constructor(private http: HttpClient) { }
  categories(): Observable<any> {
    return this.http.get('altius/categories')
  }
  brandlist(): Observable<any> {
    return this.http.get('admin/brandlist/')
  }
  brand_product_list(brandid: any): Observable<any> {
    return this.http.get('admin/brandproducts/' + brandid)
  }
  bestselling(): Observable<any> {
    return this.http.get('admin/bestsellingproducts/')
  }
  trendingproducts(): Observable<any> {
    return this.http.get('admin/trendingproducts/')
    }
    showbanner(){
      return this.http.get('admin/bannersliders/')
    }
    category_products(id:any){
      return this.http.get('admin/products/'+id)
    }
    new_arrival(){
      return this.http.get('admin/newarrivals/new')
    }
    exclusive_products(){
      return this.http.get('admin/exclusive/products')
    }
    showcart(mail:any){
      return this.http.get('mycartitems/'+mail)
    }
//product
    products(id:any){
      return this.http.get('admin/products/'+id)
    }
    product(id:any){
      return this.http.get('admin/productsattributes/'+id)
    }
    wishlist(id:any){
      return this.http.post('wishlist/addwishlist_product/',id)
    }
    compare(post:any){
      return this.http.post('admin/addtocompare/2',post)
    }
    addtocart(post:any){
      return this.http.post('carts/mine/items',post)
    }
    newsletter(email:any){
      return this.http.post('newsletters/sudhakar3@gmail.com',email)
    }
    //wishlist
    showwishlist(id:any){
      return this.http.get('admin/getwishlist/'+id)
    }
    removewishlist(id:any){
      return this.http.post('admin/removewishlists/68',id)
    }
    pricecarthide(id:any):Observable<any> {
      return	this.http.get('admin/priceaddtocartenabledisable/'+id)
      }
      configurableproduct(productid:any){
        return this.http.get('admin/productsattributes/'+productid)
      }
    
      //min/max qty
      minmax(id: any) {
        return this.http.get('admin/minmaxquantity/' + id)
      }
      //wishlist
      compareproductlist(id:any){
        return this.http.get('admin/getcompared/'+id)
      }
      deletcompare(data1:any,id:any){
        return this.http.post('admin/removecompared/'+id,data1)
      }
      customerprofile(profile:any):Observable<any>{
        return this.http.post(`customerprofile/${profile}`,[])
      
      }
      reviewcomment(productid:any){
        return this.http.get('admin/reviewandrating/'+productid)
      }
   
      bulkorder(data: any) {
        return this.http.post('bulkorder/', data)
      }
     
      cartqtyupdate(id:any,itemid:any){
        return this.http.put('carts/mine/items/'+itemid,id)
      }
      removecartitem(id: any){
        return this.http.delete('carts/mine/items/'+id)
      }
      tierprice(id:any){
        return this.http.get('admin/tierprice/'+id)
      }
      searchapi(searchdata:any){
        return this.http.get('admin/searchproduct/'+searchdata)
      }
      getsourcelist(){
        return this.http.get('admin/sourcelist/')
      }
      showsourcelist(list:any){
        return this.http.get('admin/sourceproductlist/'+list)
      }

        //quotelist
  showquotes(id: any) {
    return this.http.get('admin/addtoquoteslist/' + id)
  }
  //deletequote
  delquote(data: any) {
    return this.http.post('admin/deletequote/', data)
  }
  //quoteedit
  editquote(data: any) {
    return this.http.post('admin/editquoteconversation/', data)
  }
  //quoteconversation
  quotecon(id:any) {
    return this.http.get('admin/quotesconversations/' + id)
  }
  myorders1(email:any){
    return this.http.get('orderlistcustomer/'+email)
  }
  reorders(id:any){
    return this.http.post('admin/reorder/',id)
  }
  cancelorder(id:any){
    return this.http.post('ordercancel/31/',id)
  }
  }



 

 










