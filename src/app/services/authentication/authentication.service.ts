import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs";
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  login(post: any) {
    return this.http.post('customerlogin/id', post)
  }
  reg(post: any) {
    return this.http.post('registration/', post)
  }
  quote_id(post: any) {
    return this.http.post('carts/mine', post)
  }
}

