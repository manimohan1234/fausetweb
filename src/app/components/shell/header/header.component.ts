import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CrudService } from 'src/app/services/crud.service';
import { Product } from '../../../product.model';
import { AppState } from '../../../app.state';
import { Store } from '@ngrx/store';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  data:any=[]
  cd: any;
  logoimage: boolean=false;
  Logo: any;
  allcartlist1: any;
  card_length: number=0;
  searching!: FormGroup;
  products: any;
  search_products: any=[];
  prodlists1: any=[];
  prodlists2: any=[];
  prodlists4: any=[];
  prodlists3: any=[];
  brand: any=[];
  productlists: any=[];
  items: any;
  brands: any=[];

  constructor(public crud:CrudService, public router: Router, private toast: ToastrService,private fb:FormBuilder,private store: Store<AppState>,   private SpinnerService: NgxSpinnerService,) {
    this.searching = this.fb.group({
      search: ['', Validators.required],
    })
   
   }

  ngOnInit(): void {
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
      this.logoimage=true
     let mail=this.cd[0].email;
     let letter= mail.charAt(0);
     this.Logo =letter.toUpperCase();  
     localStorage.setItem('iniitial_logo',this.Logo)
     console.log(this.Logo)
     this.crud.showcart(mail).subscribe(res=>{
      console.log(res)
      this.allcartlist1=res;
      localStorage.setItem("cardlength",JSON.stringify(this.allcartlist1.length))
      let length=JSON.parse(localStorage.getItem("cardlength") || '{}')
       
      if(length !==0){
       this.card_length=length;
      }if(this.card_length===0){
       this.card_length=0
      }
    },(error:HttpErrorResponse)=>{
      if(error.status===401){    
        localStorage.setItem("reloginpath","/dashboard")
        this.toast.error("Session timeout please login continue");
        
     }
  
    })
    }
  }
  gotoquote(){
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
    this.router.navigate(['/pages/quotelist'])
    }else{
      this.toast.success("please login to access quotes");
    }
  }
  search(value:any){
    console.log(value.search)
    this.SpinnerService.show();
    this.search_products=[];
    this.prodlists1=[];
    this.prodlists2=[];
    this.prodlists3=[];
    this.prodlists4=[];
    this.brand=[];
    this.crud.searchapi(value.search).subscribe(res=>{
    console.log(res)
    if(res){
  
    this.search_products=res
    this.productlists = [].concat.apply([], this.search_products);
    console.log(this.productlists)
    for(let i=0;i<this.productlists.length;i++){
    this.prodlists1.push(this.productlists[i]);
    this.prodlists2.push(this.productlists[i][0])
    }
    for(let i=0;i<this.prodlists2.length;i++){
    this.prodlists3.push(this.prodlists2[i][0])
    }
    this.items=[];
    this.brands=[];
    for(let i=0;i<this.prodlists3.length;i++){
    this.items.push(this.prodlists3[i].items)
    }
    console.log(this.items)
   

this.store.dispatch({
  type: 'REMOVE_PRODUCT'
});
    this.store.dispatch({
      type: 'ADD_PRODUCT',
      payload: <Product> {
        name: this.items,
     }
     
    });
    this.SpinnerService.hide();
}})}}
