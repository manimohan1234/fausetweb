/** Angular Imports */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/** Custom Module */
import { RouterModule } from '@angular/router';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ShellComponent } from './shell.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgImageSliderModule } from 'ng-image-slider';
import { RatingModule } from 'ng-starrating';
/** Custom Material Module */
/** Shell Module */

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    ShellComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgImageSliderModule,
    RatingModule
    ],
  exports: [HeaderComponent,FooterComponent,ShellComponent,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgImageSliderModule,
    RatingModule
    
  ],
})
export class ShellModule { }
