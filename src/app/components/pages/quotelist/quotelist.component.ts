import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service'
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-quotelist',
  templateUrl: './quotelist.component.html',
  styleUrls: ['./quotelist.component.scss']
})
export class QuotelistComponent implements OnInit {
  cd: any;
  quotelist: any = [];
  nodata: any;
  pricehide: any;
  priceshow: any;
  quoteid: any;
  modalCtrl: any;
  arrayvalue: any=[];
  actionSheetController: any;
  addres: any=[];
  masterCheck: any=[];

  constructor(private crudService: CrudService,public dialog: MatDialog, private toast: ToastrService, public router: Router) { }

  ngOnInit(): void {
    this.addres == false
    this.masterCheck = false
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    let customerid = this.cd[0].customer_id
    this.crudService.showquotes(customerid).subscribe(res => {
      console.log(res)
      this.quotelist = res
      if (this.quotelist.length == 0) {
        this.nodata = true
      } else {
        this.nodata = false
      }

    }, (error: HttpErrorResponse) => {
      if (error.status === 401) {
        localStorage.setItem("reloginpath", "/quotelist")
        this.toast.info("Session timeout please login continue");
        this.router.navigate(["/login"]);
      }

    })
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if (Array.isArray(this.cd) && this.cd.length) {
      let customerid = this.cd[0].customer_id
      this.crudService.pricecarthide(customerid).subscribe(res => {
        console.log(res)
        this.pricehide = res
        console.log(this.pricehide)
        // console.log(this.pricehide[0].hide_price)
        if (Array.isArray(this.pricehide) && this.pricehide.length) {
          this.priceshow = false
        } else {
          this.priceshow = true
        }
      }, (error: HttpErrorResponse) => {
        if (error.status === 401) {
          localStorage.setItem("reloginpath", "/quotelist")
          this.toast.error("Session timeout please login continue");
          this.router.navigate(["/login"]);
        }

      })
    }
  }

  gotoquoteconversation() {
    this.router.navigate(['/pages/quoteconversation'])
  }
 editquote(con: any,qqty: any,qprice: any) {
  this.quoteid = JSON.parse(localStorage.getItem('quote_id') || '{}')

  const dialogRef = this.dialog.open(DialogData, {

      data:{
        "quote_id":this.quoteid,
        "conversation":con,
        "quote_qty":qqty,
        "quote_price":qprice
      }

  });

  /*  this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    this.quoteid = JSON.parse(localStorage.getItem('quote_id') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
      let quoteitem={
        data:{
          "quote_id":this.quoteid,
          "conversation":con,
          "quote_qty":qqty,
          "quote_price":qprice
        }
      }
      console.log(quoteitem)
      localStorage.setItem('qi',JSON.stringify(quoteitem));
             

  }else{
    this.toast.error("cannot edit quote")
  }*/
  }
  deletequote() {
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    let customerid=this.cd[0].customer_id
    this.quoteid = JSON.parse(localStorage.getItem('quote_id') || '{}')
    let quoteitem={
      data:{
        "quote_id":this.quoteid,
        "customer_id":customerid
      }
    }
    this.crudService.delquote(quoteitem).subscribe(res=>{
      console.log(res)
      if(res="Quotes Deleted"){
       this.toast.success("Quote Deleted")
       
      }else if(res="Quotes Not Deleted"){
        this.toast.error("Quotes not deleted")
        
      
    }(error:HttpErrorResponse)=>{
     if(error.status===401){          
       localStorage.setItem("reloginpath","/quotelist")
       this.toast.error("Session timeout please login continue");
       this.router.navigate(["/login"]);
    }
   
  }
  })
}
  passParams(event: any,data:any) {
    if(event.target.checked){
      this.arrayvalue.push(data);
      console.log("l",this.arrayvalue)
      }
    if (!event.target.checked) {
      let index = this.arrayvalue.indexOf(data);
    if (index > -1) {
      this.arrayvalue.splice(index, 1);
      }
    }
    console.log("hlo",this.arrayvalue)
  }

}
// Dialog Box //
@Component({
  selector: 'dialogBox',
  templateUrl: 'editquote.html',
  styleUrls: ['./quotelist.component.scss']
})
export class DialogData {
  cd: any;
  pricehide: any;
  priceshow: any;
  editquoteForm!: FormGroup;
  quoteid: any;
  age: any;
  con: any;
  quote: any=[];

  constructor(
    private crudService: CrudService,private route: ActivatedRoute, private toast: ToastrService, public router: Router,private fb:FormBuilder,
    public dialogRef: MatDialogRef<DialogData>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {
      console.log(data)
      this.quote = data;
console.log(this.quote.conversation)
       // this.da = JSON.parse(localStorage.getItem('qi') || '{}')
    //console.log("oi",this.da)
    this.editquoteForm = this.fb.group({
      quote_id: [this.quote.quote_id, Validators.required],
      conversation: [this.quote.conversation, Validators.required],
      quote_qty: [this.quote.quote_qty, Validators.required],
      quote_price: [this.quote.quote_price, Validators.required]
    })
   /* this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation()?.extras.state) {
        this.editquoteForm = this.fb.group({
          quote_id: [params.get('quote_id'), Validators.required],
          conversation: [params.get('conversation'), Validators.required],
          quote_qty: [params.get('quote_qty'), Validators.required],
          quote_price: [params.get('quote_price'), Validators.required]
        })
      }
    });*/
  }

  ngOnInit(): void {  
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if (Array.isArray(this.cd) && this.cd.length) {
      let customerid = this.cd[0].customer_id
      this.crudService.pricecarthide(customerid).subscribe(res => {
        console.log(res)
        this.pricehide = res
        console.log(this.pricehide)
        // console.log(this.pricehide[0].hide_price)
        if (Array.isArray(this.pricehide) && this.pricehide.length) {
          this.priceshow = false
        } else {
          this.priceshow = true
        }
      }, (error: HttpErrorResponse) => {
        if (error.status === 401) {
          localStorage.setItem("reloginpath", "/quotelist")
          this.toast.error("Session timeout please login continue");
          this.router.navigate(["/login"]);
        }

      })
    }
  }
  editquotes(data:any){
  data={
      data
    }
    console.log("oi",data)
    this.crudService.editquote(data).subscribe(res=>{
      console.log(res)
      if(res==="Quote Successfully updated"){
        this.toast.success("Quote Successfully updated")
        this.dialogRef.close();
      }
      else{
        alert("your quote is not updated")
        this.dialogRef.close();
      }
    },(error:HttpErrorResponse)=>{
      if(error.status===401){          
        localStorage.setItem("reloginpath","/quotelist")
        this.toast.error("Session timeout please login continue");
        this.router.navigate(["/login"]);
     }
  
    })
  }
}