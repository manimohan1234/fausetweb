import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CrudService } from 'src/app/services/crud.service'

@Component({
  selector: 'app-bulkorder',
  templateUrl: './bulkorder.component.html',
  styleUrls: ['./bulkorder.component.scss']
})
export class BulkorderComponent implements OnInit {
  checkpinform!: FormGroup;
  bulkForm!: FormGroup;
  bulk_value: any;
  quotedata: any;
  customer: any=[];
  customer1: any;
  bulkdata: any=[];
  pricehide: any=[];
  priceshow: any=[];
  pindata: any=[];
  qty: any;
  qty1: any;
  qty2: any;
  qty3: any;
  qty4: any;
 
  constructor(private fb:FormBuilder,private toast: ToastrService,private crudService: CrudService,
    private router: Router) { 
      this.bulkForm = this.fb.group({
        productSku: ['', Validators.required],
        qty:['',[Validators.required,Validators.maxLength(5),Validators.pattern('1-9')]],
        productSku1: ['', Validators.required],
        qty1:['',[Validators.required,Validators.maxLength(5)]],
        productSku2: ['', Validators.required],
        qty2:['',[Validators.required,Validators.maxLength(5)]],
        productSku3: ['', Validators.required],
        qty3:['',[Validators.required,Validators.maxLength(5)]],
        productSku4: ['', Validators.required],
        qty4:['',[Validators.required,Validators.maxLength(5)]]
      })
      
    }

  ngOnInit(): void {
    this.customer=JSON.parse(localStorage.getItem('customer')|| '[]')
    let customerid=this.customer[0].customer_id
      this.crudService.pricecarthide(customerid).subscribe(res=>{
      console.log("no",res)
      this.pricehide=res
      console.log(this.pricehide)
      if(Array.isArray(this.pricehide) && this.pricehide.length){
        this.priceshow=false
      }else{
        this.priceshow=true
      }
      })
    this.bulkForm.get(['productSku'])?.setValue(JSON.parse(localStorage.getItem('sku')|| '[]'))
    this.bulkForm.get(['qty'])?.setValue(JSON.parse(localStorage.getItem('qty')|| '[]'))
    this.bulkForm.get(['productSku1'])?.setValue(JSON.parse(localStorage.getItem('sku1')|| '[]'))
    this.bulkForm.get(['qty1'])?.setValue(JSON.parse(localStorage.getItem('qty1')|| '[]'))
    this.bulkForm.get(['productSku2'])?.setValue(JSON.parse(localStorage.getItem('sku2')|| '[]'))
    this.bulkForm.get(['qty2'])?.setValue(JSON.parse(localStorage.getItem('qty2')|| '[]'))
    this.bulkForm.get(['productSku3'])?.setValue(JSON.parse(localStorage.getItem('sku3')|| '[]'))
    this.bulkForm.get(['qty3'])?.setValue(JSON.parse(localStorage.getItem('qty3')|| '[]'))
    this.bulkForm.get(['productSku4'])?.setValue(JSON.parse(localStorage.getItem('sku4')|| '[]'))
    this.bulkForm.get(['qty4'])?.setValue(JSON.parse(localStorage.getItem('qty4')|| '[]'))   
    console.log(this.bulkForm.get(['productSku'])?.value)
    console.log(this.bulkForm.get(['productSku'])?.value+","+this.bulkForm.get(['productSku1'])?.value)
    console.log(this.bulkForm.get(['productSku'])?.value , this.bulkForm.get(['productSku1'])?.value)
  }

  
  setvalues(){
    this.qty=this.bulkForm.get(['qty'])?.value
    this.qty1=this.bulkForm.get(['qty1'])?.value
    this.qty2=this.bulkForm.get(['qty2'])?.value
    this.qty3=this.bulkForm.get(['qty3'])?.value
    this.qty4=this.bulkForm.get(['qty4'])?.value
    if(this.qty-1 < 1){
      this.bulkForm.get(["qty"])?.setValue(1)
    }
    if(this.qty1-1 < 1){
      this.bulkForm.get(["qty1"])?.setValue(1)
    }if(this.qty2-1 < 1){
      this.bulkForm.get(["qty2"])?.setValue(1)
    }if(this.qty3-1 < 1){
      this.bulkForm.get(["qty3"])?.setValue(1)
    }if(this.qty4-1 < 1){
      this.bulkForm.get(["qty4"])?.setValue(1)
    }

    localStorage.setItem('sku',JSON.stringify(this.bulkForm.value.productSku))
    localStorage.setItem('qty',JSON.stringify(this.bulkForm.value.qty))
    localStorage.setItem('sku1',JSON.stringify(this.bulkForm.value.productSku1))
    localStorage.setItem('qty1',JSON.stringify(this.bulkForm.value.qty1))
    localStorage.setItem('sku2',JSON.stringify(this.bulkForm.value.productSku2))
    localStorage.setItem('qty2',JSON.stringify(this.bulkForm.value.qty2))
    localStorage.setItem('sku3',JSON.stringify(this.bulkForm.value.productSku3))
    localStorage.setItem('qty3',JSON.stringify(this.bulkForm.value.qty3))
    localStorage.setItem('sku4',JSON.stringify(this.bulkForm.value.productSku4))
    localStorage.setItem('qty4',JSON.stringify(this.bulkForm.value.qty4))
  }
  
addtocart(value:any){
  this.customer=JSON.parse(localStorage.getItem('customer')|| '[]')
  console.log(this.customer[0].token)
  this.customer1=this.customer[0].token
  this.quotedata=JSON.parse(localStorage.getItem("quote_id")|| '[]')
  let sku=this.bulkForm.get(['productSku'])?.value
  let qty=this.bulkForm.get(['qty'])?.value
  let sku1=this.bulkForm.get(['productSku1'])?.value
  let qty1=this.bulkForm.get(['qty1'])?.value
  let sku2=this.bulkForm.get(['productSku2'])?.value
  let qty2=this.bulkForm.get(['qty2'])?.value
  let sku3=this.bulkForm.get(['productSku3'])?.value
  let qty3=this.bulkForm.get(['qty3'])?.value
  let sku4=this.bulkForm.get(['productSku4'])?.value
  let qty4=this.bulkForm.get(['qty4'])?.value
if(sku !=="" && qty !==""){
  this.bulkdata=[]
  this.bulk_value={
    data:{
    
     "sku": this.bulkForm.get(['productSku'])?.value,
     "qty":this.bulkForm.get(['qty'])?.value,
     "quote_id":this.quotedata,
     "token":this.customer1
     
    }
    
  }
  console.log(this.bulk_value)
  this.bulkdata=this.bulk_value
  console.log(this.bulkdata)
 
} if(sku !=="" && qty !=="" && sku1 !=="" && qty1 !==""){
  this.bulkdata=[]
  this.bulk_value={
    data:{
    
     "sku": this.bulkForm.get(['productSku'])?.value+","+this.bulkForm.get(['productSku1'])?.value,
     "qty":this.bulkForm.get(['qty'])?.value+","+this.bulkForm.get(['qty1'])?.value,
     "quote_id":this.quotedata,
     "token":this.customer1
     
    }
    
  }
  console.log(this.bulk_value)
  this.bulkdata=this.bulk_value
  console.log(this.bulkdata)
 
} if(sku !=="" && qty !=="" && sku1 !=="" && qty1 !=="" && sku2 !=="" && qty2 !==""){
  this.bulkdata=[]
  this.bulk_value={
    data:{
    
     "sku": this.bulkForm.get(['productSku'])?.value+","+this.bulkForm.get(['productSku1'])?.value+","+this.bulkForm.get(['productSku2'])?.value,
     "qty":this.bulkForm.get(['qty'])?.value+","+this.bulkForm.get(['qty1'])?.value+","+this.bulkForm.get(['qty2'])?.value,
     "quote_id":this.quotedata,
     "token":this.customer1
     
    }
    
  }
  console.log(this.bulk_value)
  this.bulkdata=this.bulk_value
  console.log(this.bulkdata)
  
} if(sku !=="" && qty !=="" && sku1 !=="" && qty1 !=="" && sku2 !=="" && qty2 !=="" && sku3 !=="" && qty3 !==""){
  this.bulkdata=[]
  this.bulk_value={
    data:{
    
     "sku": this.bulkForm.get(['productSku'])?.value+","+this.bulkForm.get(['productSku1'])?.value+","+this.bulkForm.get(['productSku2'])?.value+","+this.bulkForm.get(['productSku3'])?.value,
     "qty":this.bulkForm.get(['qty'])?.value+","+this.bulkForm.get(['qty1'])?.value+","+this.bulkForm.get(['qty2'])?.value+","+this.bulkForm.get(['qty3'])?.value,
     "quote_id":this.quotedata,
     "token":this.customer1
     
    }
    
  }
  console.log(this.bulk_value)
  this.bulkdata=this.bulk_value
  console.log(this.bulkdata)
 
} if(sku !=="" && qty !=="" && sku1 !=="" && qty1 !==""  && sku2 !=="" && qty2 !=="" && sku3 !=="" && qty3 !=="" && sku4 !=="" && qty4 !==""){
  this.bulkdata=[]
  this.bulk_value={
    data:{
    
     "sku": this.bulkForm.get(['productSku'])?.value+","+this.bulkForm.get(['productSku1'])?.value+","+this.bulkForm.get(['productSku2'])?.value+","+this.bulkForm.get(['productSku3'])?.value+","+this.bulkForm.get(['productSku4'])?.value,
     "qty":this.bulkForm.get(['qty'])?.value+","+this.bulkForm.get(['qty1'])?.value+","+this.bulkForm.get(['qty2'])?.value+","+this.bulkForm.get(['qty3'])?.value+","+this.bulkForm.get(['qty4'])?.value,
     "quote_id":this.quotedata,
     "token":this.customer1
     
    }
   
  }

  console.log(this.bulk_value)
  this.bulkdata=this.bulk_value
  console.log(this.bulkdata)
  
}
if(this.bulkdata.length !=0){
  this.crudService.bulkorder(this.bulkdata).subscribe(res=>{
    console.log(res)
  },(error:HttpErrorResponse)=>{
    if(error.status===401){    
      localStorage.setItem("reloginpath","/category")
      this.toast.success("Session timeout please login continue");
      this.router.navigate(["/login"]);
   }

  })
}
else{
  this.toast.success("please enter both product sku And qty")
}
  
}



  ngOnDestroy(){
    this.bulkForm.reset("");
  }
}


