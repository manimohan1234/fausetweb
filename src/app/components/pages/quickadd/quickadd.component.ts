import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-quickadd',
  templateUrl: './quickadd.component.html',
  styleUrls: ['./quickadd.component.scss']
})
export class QuickaddComponent implements OnInit {
  search_products: any=[];
  prodlists1: any=[];
  prodlists2: any=[];
  prodlists4:any=[];
  prodlists3:any=[];
  brand: any=[];
  product_show: boolean=false;
  productlists: any=[];
  items: any=[];
  brands:any=[];
  quickadd!: FormGroup;
  constructor(public crud: CrudService, private SpinnerService: NgxSpinnerService,private fb:FormBuilder) { 
    this.quickadd = this.fb.group({
     searching: ['', Validators.required],
    })
  }

  ngOnInit(): void {
  }
     search(value:any){
       console.log(value.searching)
    this.SpinnerService.show();
    this.search_products=[];
    this.prodlists1=[];
    this.prodlists2=[];
    this.prodlists3=[];
    this.prodlists4=[];
    this.brand=[];
    this.crud.searchapi(value.searching).subscribe(res=>{
    console.log(res)
    if(res){
    this.product_show=true;
    this.search_products=res
    this.productlists = [].concat.apply([], this.search_products);
    console.log(this.productlists)
    for(let i=0;i<this.productlists.length;i++){
    this.prodlists1.push(this.productlists[i]);
    this.prodlists2.push(this.productlists[i][0])
    }
    for(let i=0;i<this.prodlists2.length;i++){
    this.prodlists3.push(this.prodlists2[i][0])
    }
    this.items=[];
    this.brands=[];
    for(let i=0;i<this.prodlists3.length;i++){
    this.items.push(this.prodlists3[i].items)
    }
    console.log(this.items)
    this.SpinnerService.hide();
    }


})
  }
}
