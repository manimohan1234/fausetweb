import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-arrival-products',
  templateUrl: './arrival-products.component.html',
  styleUrls: ['./arrival-products.component.scss']
})
export class ArrivalProductsComponent implements OnInit {
  Newarrival: any=[];
  cd: any=[];

  constructor(public crud:CrudService,
    private SpinnerService: NgxSpinnerService,
    private router: Router,
    private toast: ToastrService,) { }

  ngOnInit(): void {
    this.SpinnerService.show();
    this.crud.new_arrival().subscribe(res=>{
      console.log(res)
      this.Newarrival=res
      this.SpinnerService.hide();
      })
  }
  product(id: any) {
    console.log(id)
    let navigationExtras: NavigationExtras = {
      state: {
        item: id
      }
    };
    this.router.navigate(['pages/productdetail'], navigationExtras)

  }
  addwishlist(id:any){
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
      console.log(this.cd)
      let list = {
        data:{
        "customer_id":this.cd[0].customer_id,
         "product_id":id
        }
      }
      console.log(list)
      this.crud.wishlist(list).subscribe((data:any)=> {
        console.log(data)
        if(data==="Widhlist Added"){
          this.toast.success("Widhlist Added");
        }else{
          this.toast.error("Widhlist Added Unsuccessfully");
        }
    })
  }else this.toast.error("Need login to access wishlist")
    }
    
    compare(id:any){
      this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
      if(Array.isArray(this.cd) && this.cd.length){
      console.log(this.cd)
      let com = {
        data:{
        "customer_id":this.cd[0].customer_id,
         "product_id":id
        }
      }
      this.crud.compare(com).subscribe(data => {
        console.log(data)
        this.toast.success("compare product added successfully");
    })
  }else this.toast.error("Need login to access wishlist")
    }
}
