import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrivalProductsComponent } from './arrival-products.component';

describe('ArrivalProductsComponent', () => {
  let component: ArrivalProductsComponent;
  let fixture: ComponentFixture<ArrivalProductsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArrivalProductsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrivalProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
