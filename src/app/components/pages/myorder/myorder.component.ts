import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-myorder',
  templateUrl: './myorder.component.html',
  styleUrls: ['./myorder.component.scss']
})
export class MyorderComponent implements OnInit {
  cd: any=[];
  myorder: any=[];
  nodata: boolean=true;
  detailspage: boolean=false;
  productname: any;
  image: any;
  orderid: any;
  status: any;
  paymentmethod: any;
  producturl: any;
  grandtotal: any;
  shipingamount: any;
  subtotal: any;
  totalqty: any;
  billingaddress: any;
  shipingaddress: any;
  shipingmethod: any;
  ordercurrency: any;
  customeremail: any;
  createdon: any;

  constructor(public crud: CrudService,  private toast:ToastrService,) { }

  ngOnInit(): void {
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    let email=this.cd[0].email
    this.crud.myorders1(email).subscribe(res=>{
      console.log(res)
      this.myorder=res
      if(this.myorder.length==0){
        this.nodata=true
      }else{
       this.nodata=false
      }
    })
  }
  ViewDetails(i:any){
    this.detailspage=true
    this.productname= this.myorder[i].peoduct_name
  this.image=this.myorder[i].image
  this.status=this.myorder[i].status
  this.orderid=this.myorder[i].order_id
  this.producturl=this.myorder[i].product_url
  this.paymentmethod=this.myorder[i].payment_method
  this.grandtotal=this.myorder[i].grand_total
  this.shipingamount=this.myorder[i].shipping_amount
  this.subtotal=this.myorder[i].subtotal
  this.totalqty=this.myorder[i].total_qty_ordered
  this.billingaddress=this.myorder[i].billing_address_id
  this.shipingaddress=this.myorder[i].shipping_address_id
  this.ordercurrency=this.myorder[i].order_currency_code
  this.shipingmethod=this.myorder[i].shipping_method
  this.customeremail=this.myorder[i].customer_email
  this.createdon=this.myorder[i].created_at
  }
  reorder1(){
    this.crud.reorders(this.orderid).subscribe(res=>{
      console.log(res)
      if(res="Reorder Created"){
        this.toast.success("Reorder created")
      }else{
        this.toast.error("error try later")
      }
    })
  }
  cancelorder(){
    this.crud.cancelorder(this.orderid).subscribe(res=>{
      console.log(res)
      if(res="Order Canceled"){
        this.toast.error("Order Canceled")
      }else{
        this.toast.error("not canceled try later")
      }
    })
  }
}
