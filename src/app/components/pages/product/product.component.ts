import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service'
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  productdetails: any = [];
  prodlists: any = []
  prodlists1: any = [];
  prodlists2: any = [];
  prodlists3: any = [];
  prodlists4: any = [];
  items: any = [];
  brands: any = [];
  cd: any;
  quoteid: any;
  prices1: any = [];
  prices2: any = [];
  prices3: any = [];
  prices4: any = [];
  prices5: any = [];
  term = "";
  items2: any = [];
  items3: any = [];
  items1: any = [];
  nodata: any;
  prices: any = [];
  brand: any = [];

  branddata: any = [];
  unique2: any = [];
  keys: any = [];
  unique: any = [];
  attribList: any = [];
  attrval: any = [];
  attributes1!: { data1: { value: any; }; };
  val: any = [];
  checkboxvalue: any = [];
  attributes!: { data: { title: any; valuess: any[]; checkboxval: any; }; };
  value: any = []
  vall: any = [];
  valll: any = [];
  records: any = [];
  arrayvalue: any = [];
  isPresent: any;
  constructor(private crudService: CrudService, private SpinnerService: NgxSpinnerService, private toast: ToastrService, public router: Router, private route: ActivatedRoute) {
    this.SpinnerService.show();
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation()?.extras.state) {
        let data1 =JSON.parse(this.router.getCurrentNavigation()?.extras.state?.item);
        this.products(data1)
      }else{
     this.products(localStorage.getItem('pid') || '()')
      }
    })
  }

  ngOnInit(): void {

  }
  products(data1: any) {
    this.nodata = true
    this.items = [];
    this.items1 = [];
    this.items2 = [];
    this.items3 = [];
    this.prodlists = [];
    this.prodlists1 = [];
    this.prodlists2 = [];
    this.prodlists3 = [];
    this.prodlists4 = [];
    this.crudService.products(data1).subscribe(data => {

      this.productdetails = data;
      this.prodlists = [].concat.apply([], this.productdetails);
      console.log(this.prodlists)
      for (let i = 0; i < this.prodlists.length; i++) {
        this.prodlists1.push(this.prodlists[i]);
        this.prodlists2.push(this.prodlists1[i][0])
      }
      for (let i = 0; i < this.prodlists2.length; i++) {
        this.prodlists3.push(this.prodlists2[i][0])
        this.prodlists4.push(this.prodlists2[i][1])
      }
      this.items = [];
      this.brands = [];
      for (let i = 0; i < this.prodlists3.length; i++) {
        this.items.push(this.prodlists3[i].items)
      }
      for (let i = 0; i < this.prodlists4.length; i++) {
        this.brands.push(this.prodlists4[i])
      }
      console.log(this.items)
      console.log(this.brands)
      for (let i = 0; i < this.items.length; i++) {
        this.items3.push(this.items[i])
      }
      this.items2 = this.items3
      if (this.items3.length != 0) {
        this.nodata = true
      } else {
        this.nodata = false
      }
      console.log(this.items3)
      this.prices = [];
      for (let i = 0; i < this.items.length; i++) {
        this.prices.push(this.items3[i].price)
      }
     // this.starrating();
      this.SpinnerService.hide();

      for (let i = 0; i < this.brands.length; i++) {
        this.brand.push(this.brands[i])
      }
      //console.log(this.brand[1])
      // console.log(this.items[0].id)
      // console.log(this.brand)
      this.branddata = [];
      Object.entries(this.brand).map(([key, val], index) => {
        //  console.log(key)
        //  console.log(val)
        this.vall = val
        Object.entries(this.vall).map(([key, value], ind) => {
          this.valll = value
          Object.entries(this.valll).map(([keys, values], ind) => {
            this.branddata.push({ keys, values });
            // console.log(keys)
          })
        })
      })
      this.unique2 = [];
      let branss = this.branddata
      this.unique2 = branss.filter((item: any, i: any, ar: any) => ar.indexOf(item) === i);
      // console.log(this.unique2)
      this.keys = []
      for (let i = 0; i < this.unique2.length; i++) {
        this.keys.push(this.unique2[i].keys);
      }
      // console.log(this.keys)
      let names = this.keys
      this.unique = names.filter((item: any, i: any, ar: any) => ar.indexOf(item) === i);
      // console.log(this.unique);
      this.attribList = [];
      this.attrval = []
      // console.log(this.unique2.length)
      for (let i = 0; i < this.unique.length; i++) {
        this.attrval = [];
        for (let j = 0; j < this.unique2.length; j++) {

          if (this.unique2[j]["keys"] === this.unique[i]) {
            let list = this.unique2[j]["values"]
            this.attrval.push(list)
          }
        }
        // console.log(this.attrval)
        if (Array.isArray(this.attrval) && this.attrval.length) {

          let newval = this.attrval.filter((item, i, ar) => ar.indexOf(item) === i);
          for (let k = 0; k < newval.length; k++) {
            this.val = false
            this.attributes1 = {
              data1: {
                value: this.val,
              }
            }
            this.checkboxvalue.push(this.attributes1)

          }
          this.attributes = {
            data: {
              "title": this.unique2[i].keys,
              "valuess": newval,
              "checkboxval": this.checkboxvalue,
            }
          }
          this.attribList.push(this.attributes)

        }
        this.attrval = [];
        this.checkboxvalue = [];

      }
      console.log(this.attribList)

    })
  }

  passParams(event: any, data: any) {
    this.records = [];
    if (event.target.checked) {
      this.arrayvalue.push(data);
      console.log(this.arrayvalue)
      for (let j = 0; j < this.items3.length; j++) {
        this.isPresent = true;
        for (let i = 0; i < this.arrayvalue.length; i++) {
          this.isPresent = true;
          if ((this.items3[j]["name"].indexOf(this.arrayvalue[i]) === -1) && (this.items3[j]["description"].indexOf(this.arrayvalue[i]) === -1) && (this.items3[j]["description"].indexOf(this.arrayvalue[i]) === -1)) {
            this.isPresent = false;

          }

          if (this.isPresent == true) {
            this.records.push(this.items3[j]);
            this.items2 = this.records
          }

        }
      }
    }
    console.log(this.records)
    if (!event.target.checked) {
      let index = this.arrayvalue.indexOf(data);
      if (index > -1) {
        this.arrayvalue.splice(index, 1);
        for (let j = 0; j < this.items3.length; j++) {
          this.isPresent = true;
          for (let i = 0; i < this.arrayvalue.length; i++) {
            this.isPresent = true;
            if ((this.items3[j]["name"].indexOf(this.arrayvalue[i]) === -1) && (this.items3[j]["description"].indexOf(this.arrayvalue[i]) === -1) && (this.items3[j]["description"].indexOf(this.arrayvalue[i]) === -1)) {
              this.isPresent = false;

            }

            if (this.isPresent == true) {
              this.records.push(this.items3[j]);
              this.items2 = this.records
            }

          }
        }
      }
      if (this.arrayvalue.length == 0) {
        this.items2 = this.items3;
      }
    }
    console.log(this.arrayvalue)
  }

  addwishlist(id: any) {
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if (Array.isArray(this.cd) && this.cd.length) {
      console.log(this.cd)
      let list = {
        data: {
          "customer_id": this.cd[0].customer_id,
          "product_id": id
        }
      }
      console.log(list)
      this.crudService.wishlist(list).subscribe((data: any) => {
        console.log(data)
        if (data === "Widhlist Added") {
          this.toast.success("Widhlist Added");
        } else {
          this.toast.error("Widhlist Added Unsuccessfully");
        }
      })
    } else this.toast.error("Need login to access wishlist")
  }

  compare(id: any) {
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if (Array.isArray(this.cd) && this.cd.length) {
      console.log(this.cd)
      let com = {
        data: {
          "customer_id": this.cd[0].customer_id,
          "product_id": id
        }
      }
      this.crudService.compare(com).subscribe(data => {
        console.log(data)
        this.toast.success("compare product added successfully");
      })
    } else this.toast.error("Need login to access wishlist")
  }

  addtocart(sku: any) {
    this.quoteid = JSON.parse(localStorage.getItem('quote_id') || '{}')
    console.log(this.quoteid)
    let cart = {
      cartItem: {
        "sku": sku,
        "qty": "1",
        "quote_id": this.quoteid
      }
    }
    this.crudService.addtocart(cart).subscribe(res => {
      console.log(res)
      if (res != "") {
        this.toast.success("cart added successfully")
      }
    })
  }

presentActionSheet(event: any) {
    console.log(event.target.value)
    if (event.target.value == 1) {
      this.items.sort(this.dynamicSort("name"));
    } else if (event.target.value == 2) {
      this.items.sort(this.dynamicSort("-name"));
      console.log(this.items)
    } else if (event.target.value == 3) {
      this.items.sort(this.dynamicSort("price"));
    } else {
      this.items.sort(this.dynamicSort("-price"));
      console.log(this.items)
    }
  }
  dynamicSort(property: any) {
    var sortOrder = 1;

    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }

    return function (a: any, b: any) {
      if (sortOrder == -1) {
        return b[property].localeCompare(a[property]);
      } else {
        return a[property].localeCompare(b[property]);
      }
    }
  }
  /*  starrating() {
    let data = 99.99;
    this.prices1 = []
    for (let i = 0; i < this.items3.length; i++) {
      if (parseFloat(this.items3[i].price) < data) {
        this.prices1.push(this.items3[i])
      }
    }
    let data2 = 199.99;
    let data1 = 100.00;
    this.prices2 = []
    for (let i = 0; i < this.items3.length; i++) {
      if (parseFloat(this.items3[i].price) < data2 && parseFloat(this.items3[i].price) > data1) {
        this.prices2.push(this.items3[i])
      }
    }
    let data3 = 299.99;
    let data4 = 200.00;
    this.prices3 = []
    for (let i = 0; i < this.items3.length; i++) {
      if (parseFloat(this.items3[i].price) < data3 && parseFloat(this.items3[i].price) > data4) {
        this.prices3.push(this.items3[i])
      }
    }
    let data5 = 399.99;
    let data6 = 300.00;
    this.prices3 = []
    for (let i = 0; i < this.items3.length; i++) {
      if (parseFloat(this.items3[i].price) < data5 && parseFloat(this.items3[i].price) > data6) {
        this.prices5.push(this.items3[i])
      }
    }
    let data7 = 400.00;
    this.prices4 = []
    for (let i = 0; i < this.items3.length; i++) {
      if (parseFloat(this.items3[i].price) > data7) {
        this.prices4.push(this.items3[i])
      }
    }
  }
  filtering(name: any) {
    if (name == "99.99" || name == "199.99" || name == "299.99" || name == "399.99" || name == "400.00") {
      if (name == "99.99") {
        let data = 99.99;
        this.prices1 = []
        for (let i = 0; i < this.items.length; i++) {
          if (this.items[i].price < data) {
            this.prices1.push(this.items[i])
          }
        }
        console.log(this.prices1)
        this.items2 = this.prices1;
      }
      else if (name == "199.99") {
        let data = 199.99;
        let data1 = 100.00;
        this.prices2 = []
        for (let i = 0; i < this.items.length; i++) {
          if (this.items[i].price < data && this.items[i].price > data1) {
            this.prices2.push(this.items[i])
          }
        }
        this.items2 = this.prices2;
      }

      else if (name == "299.99") {
        let data = 299.99;
        let data1 = 200.00;
        this.prices3 = []
        for (let i = 0; i < this.items.length; i++) {
          if (this.items[i].price < data && this.items[i].price > data1) {
            this.prices3.push(this.items[i])
          }
        }
        this.items2 = this.prices3;
      } else if (name == "399.99") {
        let data = 399.99;
        let data1 = 300.00;
        this.prices4 = []
        for (let i = 0; i < this.items.length; i++) {
          if (this.items[i].price < data && this.items[i].price > data1) {
            this.prices4.push(this.items[i])
          }
        }
        this.items2 = this.prices4;
      }

      else if (name == "400.00") {
        let data = 400.00;
        this.prices5 = []
        for (let i = 0; i < this.items.length; i++) {
          if (this.items[i].price > data) {
            this.prices5.push(this.items[i])
          }
        }
        this.items2 = this.prices5;
      }
    }
    else {
      this.term = name;
      console.log(name)
    }
  }*/
  product(id: any) {
    console.log(id)
    let navigationExtras: NavigationExtras = {
      state: {
        item: id
      }
    };
    this.router.navigate(['pages/productdetail'], navigationExtras)

  }
}


