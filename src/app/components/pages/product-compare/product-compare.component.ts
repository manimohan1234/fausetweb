    import { Component, OnInit } from '@angular/core';
    import { Router } from '@angular/router';
    import { NgxSpinnerService } from 'ngx-spinner';
    import { ToastrService } from 'ngx-toastr';
    import { CrudService } from 'src/app/services/crud.service';

    @Component({
    selector: 'app-product-compare',
    templateUrl: './product-compare.component.html',
    styleUrls: ['./product-compare.component.scss']
    })
    export class ProductCompareComponent implements OnInit {
    cd: any=[];
    items: any=[];
    testObject: any=[];
    items1: any=[];
    items3: any=[];
    data: any=[];
    quoteid: any;
    objectKeys = Object.keys;
    data1:any=[]
    constructor(public crud:CrudService,
    private SpinnerService: NgxSpinnerService,
    private toast: ToastrService,
    private router: Router) { }

    ngOnInit(): void {
    this.SpinnerService.show()
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
    let customerid1=this.cd[0].customer_id
    this.crud.compareproductlist(customerid1).subscribe(res=>{
    console.log(res)
    this.items=res
    this.SpinnerService.hide()

    if(res !=""){
    console.log(this.items[0].attributes)
    for(let i=0;i<this.items.length;i++){
    this.testObject.push( this.items[i].attributes)      
    }
    console.log(this.testObject)
    }
    else{
    alert("no data")
    }
    for(let i=0;i<this.items.length;i++){
    Object.entries(this.testObject[i]).map(([key,val],index)=>{
    this.data=val
    Object.entries(this.data).map(([key,value],ind)=>{
    this.items1.push({key,value});
    })
    })

    const result=Array.from(new Set(this.items1.map((s: { key: any; })=>s.key)))
    .map(key=>{
    return{
    key:key,
    value:this.items1.find((s: { key: any; })=>s.key===key).value
    }
    },
    )

    this.items3.push(result)
    this.items1=[];
    }
    console.log(this.items3)
    })

    }
    }
    
    addwishlist(id:any){
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
    console.log(this.cd)
    let list = {
    data:{
    "customer_id":this.cd[0].customer_id,
    "product_id":id
    }
    }
    console.log(list)
    this.crud.wishlist(list).subscribe((data:any)=> {
    console.log(data)
    if(data==="Widhlist Added"){
    this.toast.success("Widhlist Added");
    }else{
    this.toast.error("Widhlist Added Unsuccessfully");
    }
    })
    }else this.toast.error("Need login to access wishlist")
    }
    addtocart(sku:any){
    this.quoteid = JSON.parse(localStorage.getItem('quote_id') || '{}')
    console.log(this.quoteid)
    let cartItem1={ 
    cartItem: {
    "sku": sku,
    "qty": "1",
    "quote_id": this.quoteid
    }
    }

    this.crud.addtocart(cartItem1).subscribe(res=>{
    console.log(res)
    if(res !=""){
    this.toast.success("cart added successfully")
    }
    })
    }

    delete(id:any){
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')

    let customerid=this.cd[0].customer_id
    this.data1={
    data:{"customer_id" :customerid,
    "product_id": id,}

    }
    this.crud.deletcompare(this.data1,customerid).subscribe(res=>{
    console.log(res)
    this.ngOnInit();
    this.toast.success("product removed successfully")
    })
    }
    }



