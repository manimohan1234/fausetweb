import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss']
})
export class WishlistComponent implements OnInit {
  cd: any=[];
  allwishlist1: any=[];
  quoteid: any=[];
  data1: any=[];
  pricehide: any;
  priceshow: boolean=false;

  constructor(private crudService: CrudService,private SpinnerService: NgxSpinnerService, private toast: ToastrService, public router: Router) { }

  ngOnInit(): void {
    this.SpinnerService.show();
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    this.crudService.showwishlist(this.cd[0].customer_id).subscribe(res=>{
      console.log(res)
      this.allwishlist1=res;
      this.crudService.pricecarthide(this.cd[0].customer_id).subscribe(res=>{
        console.log(res)
        this.pricehide=res
        console.log(this.pricehide)
        // console.log(this.pricehide[0].hide_price)
        if(Array.isArray(this.pricehide) && this.pricehide.length){
          this.priceshow=false
        }else{
          this.priceshow=true
        }
        },(error:HttpErrorResponse)=>{
          if(error.status===401){         
            localStorage.setItem("reloginpath","/wishlist")
            this.toast.success("Session timeout please login continue");
            this.router.navigate(["/login"]);
         }
      
        })
      this.SpinnerService.hide();
    })
  }

  addtocart(sku:any){
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
    this.quoteid = JSON.parse(localStorage.getItem('quote_id') || '{}')
 console.log(this.quoteid)
   let cartItem1={ 
     cartItem: {
      "sku": sku,
      "qty": "1",
      "quote_id": this.quoteid
   }
  }
   
 this.crudService.addtocart(cartItem1).subscribe(res=>{
   console.log(res)
   if(res !=""){
     this.toast.success("cart added successfully")
   }
 })
  }
  else this.toast.error("Need login to access cart")
}

removewishlist(id:any){
  this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
  let customerid=this.cd[0].customer_id
  this.data1={
   data:{"customer_id" :customerid,
   "product_id": id,}
   
    }
    
  this.crudService.removewishlist(this.data1).subscribe(res=>{
    console.log(res)
   
   this.toast.success("removed successfully");
   this.ngOnInit();
  })

}

  }


