import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { CrudService } from 'src/app/services/crud.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-mycart',
  templateUrl: './mycart.component.html',
  styleUrls: ['./mycart.component.scss']
})
export class MycartComponent implements OnInit {
  cd: any;
  logoimage: boolean = false;
  Logo: any;
  allcartlist1: any = [];
  value: any = [];
  allminmax: any = [];
  quotedata: any = [];
  minmax: any = [];
  data1: any = []
  customer: any;
  tierprices: any = [];
  alltierprices: any;
  price: number = 0;
  showSelected: boolean = false;

  show: any;
  constructor(public crud: CrudService, private fb: FormBuilder, private SpinnerService: NgxSpinnerService, private toast: ToastrService, public router: Router, private route: ActivatedRoute, private crudService: CrudService,) {
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation()?.extras.state) {
        let items = this.router.getCurrentNavigation()?.extras.state?.item;
        console.log(items)
      }
    })

  }

  ngOnInit(): void {
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if (Array.isArray(this.cd) && this.cd.length) {
      this.logoimage = true
      let mail = this.cd[0].email;
      let letter = mail.charAt(0);
      this.Logo = letter.toUpperCase();
      localStorage.setItem('iniitial_logo', this.Logo)
      console.log(this.Logo)
      this.crud.showcart(mail).subscribe(res => {
        console.log(res)
        this.allcartlist1 = res;

        for (let i = 0; i < this.allcartlist1?.length; i++) {
          this.price += this.allcartlist1[i].price * this.allcartlist1[i].qty
        }
        for (let i = 0; i < this.allcartlist1?.length; i++) {

          this.crudService.minmax(this.allcartlist1[i].product_id).subscribe(res => {
            console.log("mm", res)
            this.value = res
            if (res != "") {
              this.allminmax.push(this.value[0])
              console.log(this.allminmax)
            }
          }, (error: HttpErrorResponse) => {
            if (error.status === 401) {
              localStorage.setItem("reloginpath", "/cart")
              this.toast.success("Session timeout please login continue");
              this.router.navigate(["/login"]);
            }
          })
        }
      }, (error: HttpErrorResponse) => {
        if (error.status === 401) {
          localStorage.setItem("reloginpath", "/dashboard")
          this.toast.error("Session timeout please login continue");

        }

      })

      this.alltierprices = [];
      for (let i = 0; i < this.allcartlist1?.length; i++) {
        this.crudService.tierprice(this.allcartlist1[i].product_id).subscribe(res => {
          console.log(res)
          if (res != "") {
            this.alltierprices.push("tierprice")

            console.log(this.alltierprices)
          } else {
            this.alltierprices.push(i)
          }
        })
      }
    }

  }

  totalcal() {
    this.price = 0
    for (let i = 0; i < this.allcartlist1?.length; i++) {
      this.price += this.allcartlist1[i].price * this.allcartlist1[i].qty
    }
  }
  incrementQty1(i: any, id: any) {
    this.customer = JSON.parse(localStorage.getItem('customer') || '[]')
    if (Array.isArray(this.customer) && this.customer.length) {
      let itemid1 = this.allcartlist1[i].product_id
      this.crudService.tierprice(itemid1).subscribe(res => {
        console.log("hii", res)
        if (res != "") {
          this.tierprices = res
          this.toast.success("You cannot edit tier price product")
        } else {
          this.incrementQty(i);
        }
      })
    }
  }
  decreaseQty1(i: any, id: any) {
    this.customer = JSON.parse(localStorage.getItem('customer') || '[]')
    if (Array.isArray(this.customer) && this.customer.length) {
      let itemid1 = this.allcartlist1[i].product_id
      this.crudService.tierprice(itemid1).subscribe(res => {
        console.log(res)
        if (res != "") {
          this.tierprices = res
        } else {
          this.decreaseQty(i);
        }
      })
    }
  }
  incrementQty(i: any) {

    this.quotedata = JSON.parse(localStorage.getItem("quote_id") || '[]')
    console.log("oi", this.quotedata)
    let itemid1 = this.allcartlist1[i].product_id
    this.crudService.minmax(itemid1).subscribe(res => {
      console.log("hey", res)
      this.minmax = res
    }, (error: HttpErrorResponse) => {
      if (error.status === 401) {
        localStorage.setItem("reloginpath", "/cart")
        this.toast.success("Session timeout please login continue");
        this.router.navigate(["/login"]);
      }

    })
    if (this.allcartlist1[i].qty <= this.allminmax[i]?.min_sale_qty) {
      this.toast.success("you have reached min qty for this product")
    }
    if (this.allcartlist1[i].qty >= this.allminmax[i]?.max_sale_qty) {
      this.toast.success("you have reached max qty for this product")
    }
    let a = this.allminmax[i]?.max_sale_qty;
    // this.minmax[0].max_sale_qty
    console.log(a)
    if (a > this.allcartlist1[i].qty) {
      this.allcartlist1[i].qty += 1;
      let itemid = this.allcartlist1[i].item_id
      console.log("lo", this.allcartlist1[i].qty)
      this.data1 = {

        cartItem: {
          "item_id": itemid,
          "qty": this.allcartlist1[i].qty,
          "quote_id": this.quotedata
        }

      }

      this.crudService.cartqtyupdate(this.data1, itemid).subscribe(res => {
        console.log(res)
        this.totalcal()
        console.log(this.allcartlist1[i].qty);
      }, (error: HttpErrorResponse) => {
        if (error.status === 401) {
          localStorage.setItem("reloginpath", "/cart")
          this.toast.success("Session timeout please login continue");
          this.router.navigate(["/login"]);
        }

      })
    } else {
      // this.minmax[0].max_sale_qty
      let b = this.minmax[i]?.max_sale_qty;
      this.allcartlist1[i].qty = b
      this.toast.success("you reached maximum quantity")
      alert("you cannot add more than max qty")
      let itemid = this.allcartlist1[i].item_id
      console.log("lll", this.allcartlist1[i].item_id)
      this.data1 = {
        cartItem: {
          "item_id": itemid,
          "qty": this.allcartlist1[i].qty,
          "quote_id": this.quotedata
        }
      }

      this.crudService.cartqtyupdate(this.data1, itemid).subscribe(res => {
        console.log(res)
        this.totalcal()
      }, (error: HttpErrorResponse) => {
        if (error.status === 401) {
          localStorage.setItem("reloginpath", "/cart")
          this.toast.success("Session timeout please login continue");
          this.router.navigate(["/login"]);
        }

      })
    }
  }
  decreaseQty(i: any) {
    let itemid1 = this.allcartlist1[i].product_id
    this.crudService.minmax(itemid1).subscribe(res => {
      console.log(res)
      this.minmax = res
    }, (error: HttpErrorResponse) => {
      if (error.status === 401) {
        localStorage.setItem("reloginpath", "/cart")
        this.toast.success("Session timeout please login continue");
        this.router.navigate(["/login"]);
      }

    })
    if (this.allcartlist1[i].qty <= this.allminmax[i]?.min_sale_qty) {
      this.toast.success("you have reached min qty for this product")
    }
    if (this.allcartlist1[i].qty >= this.allminmax[i]?.max_sale_qty) {
      this.toast.success("you have reached max qty for this product")
    }
    if (this.allcartlist1[i].qty - 1 < this.allminmax[i]?.min_sale_qty) {
      this.allcartlist1[i].qty = parseFloat(this.allminmax[i]?.min_sale_qty)
      // this.minmax[0].min_sale_qty;
      console.log(this.allcartlist1[i].qty)
      this.totalcal()
    }
    else {
      this.quotedata = JSON.parse(localStorage.getItem("quote_id") || '[]')
      console.log(this.quotedata)

      this.allcartlist1[i].qty -= 1;
      let itemid = this.allcartlist1[i].item_id
      this.data1 = {
        cartItem: {
          "item_id": itemid,
          "qty": 0,
          "quote_id": this.quotedata
        }
      }
      this.crudService.cartqtyupdate(this.data1, itemid).subscribe(res => {
        console.log(res)
        this.totalcal()
        console.log(this.allcartlist1[i].qty);
      }, (error: HttpErrorResponse) => {
        if (error.status === 401) {
          localStorage.setItem("reloginpath", "/cart")
          this.toast.success("Session timeout please login continue");
          this.router.navigate(["/login"]);
        }

      })
    }
  }
  checkqty(i: any) {

    console.log(this.allminmax[i]?.min_sale_qty)
    if (this.allcartlist1[i].qty > this.allminmax[i]?.min_sale_qty && this.allcartlist1[i].qty < this.allminmax[i]?.max_sale_qty) {
      this.toast.success("you can purchase this product now")
    } else {
      this.toast.success("you didnt reached min/max qty for this product")
    }
  }

  removecart(id: any) {
    this.crudService.removecartitem(id).subscribe(res => {
      console.log(res)
      this.toast.success("removed successfully");
      this.ngOnInit();
    })
  }

  gotocheckout() {
    alert("checkout")
  }

  flip() {
    this.showSelected = !this.showSelected;
  }

}
