/** Angular Imports */
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Shell } from "../../services/core/shell.service";

/** Custom Component */
import { DashboardComponent } from '../pages/dashboard/dashboard.component';
import { ArrivalProductsComponent } from './arrival-products/arrival-products.component';
import { ExclusiveProductsComponent } from './exclusive-products/exclusive-products.component';
import { MyaccountComponent } from './myaccount/myaccount.component';
import { MycartComponent } from './mycart/mycart.component';
import { ProductCompareComponent } from './product-compare/product-compare.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { ProductComponent } from './product/product.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { BulkorderComponent } from './bulkorder/bulkorder.component';
import { BrandListComponent } from './brand-list/brand-list.component';
import { QuickaddComponent } from './quickadd/quickadd.component';
import { StorelistComponent } from './storelist/storelist.component';
import { QuotelistComponent } from './quotelist/quotelist.component';
import { QuoteconversationComponent } from './quoteconversation/quoteconversation.component';
import { MyorderComponent } from './myorder/myorder.component';
import { SearchComponent } from './search/search.component';

/** Customer Routes */
const routes: Routes = [
  Shell.childRoutes([
    { path: 'dashboard', component: DashboardComponent },
    { path: 'product', component: ProductComponent },
    { path: 'productdetail', component: ProductDetailComponent },
    { path: 'new_arrival', component: ArrivalProductsComponent },
    { path: 'exclusive_product', component: ExclusiveProductsComponent },
    { path: 'wishlist', component: WishlistComponent },
    { path: 'mycart', component: MycartComponent },
    { path: 'myaccount', component: MyaccountComponent },
    { path: 'bulkorder', component: BulkorderComponent },
    { path: 'product_compare', component: ProductCompareComponent },
    { path: 'brand_list', component: BrandListComponent },
    { path: 'quickadd', component: QuickaddComponent },
    { path: 'storelist', component: StorelistComponent },
    { path: 'quotelist', component: QuotelistComponent },
    { path: 'quoteconversation', component: QuoteconversationComponent },
    { path: 'myorder', component: MyorderComponent },
    { path: 'search', component: SearchComponent },
    ])
  

];


/** Customer Routing Module */
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }
