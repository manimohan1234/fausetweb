import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NavigationExtras, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { StarRatingComponent } from 'ng-starrating';
import { Validators,FormGroup ,FormBuilder} from '@angular/forms';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  brands: any=[];
  trending: any=[];
  bestselling: any=[];
  bselling: any=[];
  categories: any;
  categories1: any;
  categories2: any=[];
  banner: any=[];
  imageObject: any=[];
  cd: any=[];
  productdetails: any=[];
  prodlists: any=[];
  prodlists1: any=[];
  prodlists2: any=[];
  prodlists3: any=[];
  prodlists4: any=[];
  items: any=[];
  Newarrival: any=[];
  exclusive: any=[];
  quoteid: any;
  newsletterForm: FormGroup;
  logoimage: boolean=false;
  Logo: any;
  allcartlist1: any=[];
  card_length:number=0;

  constructor(public crud:CrudService,
    private SpinnerService: NgxSpinnerService,
    private router: Router,
    private toast: ToastrService,
    public fb:FormBuilder
  ) { 
    this.newsletterForm = this.fb.group({
      email: ['',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]]
    })
  }

  ngOnInit(): void {
    this.SpinnerService.show();
    this.crud.brandlist().subscribe((res:any)=>{
    this.brands=res
    })
    this.crud.trendingproducts().subscribe(res=>{
    this.trending=res
    console.log(res)
    })

  
    
   
    this.bestselling=[]
    this.crud.bestselling().subscribe(res=>{
      console.log(res)
    this.bselling=res
    for(let i=0;i<8;i++){
    this.bestselling.push(this.bselling[i])
    }
   })
    this.crud.categories().subscribe(res=>{
    this.categories=res
    console.log(this.categories.children_data[0].children_data)
    this.categories1=this.categories.children_data[0].children_data
    this.categories2=[];
    for(let i=0;i<4;i++){
      if(this.categories1[i]){
        this.categories2.push(this.categories1[i])
      }
  }
    this.items=[];
    this.prodlists = [];
    this.prodlists1 = [];
    this.prodlists2 = [];
    this.prodlists3 = [];
    this.prodlists4 = [];
    this.selectab(this.categories2[0].id)
    
  
    },(error:HttpErrorResponse)=>{
    if(error.status===401){    
      this.SpinnerService.hide();
    this.toast.error("Session timeout please login continue");
    this.router.navigate(["/login"]);
    }

    })
    this.crud.showbanner().subscribe(res=>{
    console.log(res)
    this.banner=res
    console.log( this.banner[0].banner_image)
  
    this.SpinnerService.hide();
    },(error:HttpErrorResponse)=>{
    if(error.status===401){       
      this.SpinnerService.hide();   
    localStorage.setItem("reloginpath","/dashboard")
    this.toast.error("Session timeout please login continue");
    this.router.navigate(["/login"]);
    }
    
    })
 
    }
    onRate($event:{oldValue:number, newValue:number, starRating:StarRatingComponent}) {
      alert(`Old Value:${$event.oldValue}, 
        New Value: ${$event.newValue}, 
        Checked Color: ${$event.starRating.checkedcolor}, 
        Unchecked Color: ${$event.starRating.uncheckedcolor}`);
    }
    openbrand(){

    }
    selectcategory(id:any,category:any){
    if(category.children_data.length==0){
    console.log(id)
    localStorage.setItem("pid",id)
     let navigationExtras: NavigationExtras = {
        state: {
          item: JSON.stringify(id)
        }
      };
    this.router.navigate(['pages/product'],navigationExtras)
    
      }
    }
    addwishlist(id:any){
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
      console.log(this.cd)
      let list = {
        data:{
        "customer_id":this.cd[0].customer_id,
         "product_id":id
        }
      }
      console.log(list)
      this.crud.wishlist(list).subscribe((data:any)=> {
        console.log(data)
        if(data==="Widhlist Added"){
          this.toast.success("Widhlist Added");
        }else{
          this.toast.error("Widhlist Added Unsuccessfully");
        }
    })
  }else this.toast.error("Need login to access wishlist")
    }
    
    compare(id:any){
      this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
      if(Array.isArray(this.cd) && this.cd.length){
      console.log(this.cd)
      let com = {
        data:{
        "customer_id":this.cd[0].customer_id,
         "product_id":id
        }
      }
      this.crud.compare(com).subscribe(data => {
        console.log(data)
        this.toast.success("compare product added successfully");
    })
  }else this.toast.error("Need login to access wishlist")
    }
    addtocart(sku:any){
      this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
      if(Array.isArray(this.cd) && this.cd.length){

       this.quoteid = JSON.parse(localStorage.getItem('quote_id') || '{}')
    console.log(this.quoteid)
      let cartItem1={ 
        cartItem: {
         "sku": sku,
         "qty": "1",
         "quote_id": this.quoteid
      }
     }
      
    this.crud.addtocart(cartItem1).subscribe(res=>{
      console.log(res)
      if(res !=""){
        this.toast.success("cart added successfully")
      }
    })
  }else this.toast.error("Need login to access cart")
     }

     newsletter(value:any){
      this.crud.newsletter(value).subscribe(res=>{
        console.log(res)
        if(res === "This email address is already subscribed."){
        this.toast.error("This email address is already subscribed");
        }else{
       this.toast.success("Thankyou for your subscription")
        }
        this.newsletterForm.reset("");
      })
    }
     selectab(id:any){
       console.log(id)
      this.items.push("");
      this.prodlists = [];
      this.prodlists1 = [];
      this.prodlists2 = [];
      this.prodlists3 = [];
      this.prodlists4 = [];
      
       this.SpinnerService.show();
    
      this.crud.category_products(id).subscribe(res=>{
        console.log(res)
        this.productdetails = res;
        this.prodlists = [].concat.apply([], this.productdetails);
        for (let i = 0; i < this.prodlists.length; i++) {
          this.prodlists1.push(this.prodlists[i]);
          this.prodlists2.push(this.prodlists1[i][0])
        }
        for (let i = 0; i < this.prodlists2.length; i++) {
          this.prodlists3.push(this.prodlists2[i][0])
          this.prodlists4.push(this.prodlists2[i][1])
        }
        this.items = [];
        
        for (let i = 0; i < this.prodlists3.length; i++) {
          this.items.push(this.prodlists3[i].items)
        }
      
        console.log(this.items)
        this.SpinnerService.hide();
      })
     }

     newarrival(){
       this.router.navigate(['/pages/new_arrival'])
     }
     exclusiveproduct(){
      this.router.navigate(['/pages/exclusive_product'])
     }
    product(id: any) {
    console.log(id)
    let navigationExtras: NavigationExtras = {
      state: {
        item: id
      }
    };
    this.router.navigate(['pages/productdetail'], navigationExtras)

  }

  selectbrand(brand_id:any){
    localStorage.setItem("brandsid",brand_id)
    let navigationExtras: NavigationExtras = {
      state: {
     brandid:brand_id
      }
    };
    this.router.navigate(["pages/brand_list"],navigationExtras)
  }
  }