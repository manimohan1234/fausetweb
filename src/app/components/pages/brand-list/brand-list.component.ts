import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-brand-list',
  templateUrl: './brand-list.component.html',
  styleUrls: ['./brand-list.component.scss']
})
export class BrandListComponent implements OnInit {
  brand: any=[];
  brandsproduct: any;
  cd: any;

  constructor(private router:Router,private route: ActivatedRoute,public crud:CrudService,
    private SpinnerService: NgxSpinnerService,   private toast: ToastrService,) {
    this.route.queryParams.subscribe(params => {
      this.SpinnerService.show();
      if (this.router.getCurrentNavigation()?.extras.state) {
      this.brand= this.router.getCurrentNavigation()?.extras?.state?.brandid;
      if (this.router.getCurrentNavigation()?.extras.state) {
      this.crud.brand_product_list(this.brand).subscribe(res=>{
        console.log(res)
        this.brandsproduct=res
        this.SpinnerService.hide();
      })
    }
  
    }
    else{
      this.crud.brand_product_list(localStorage.getItem("brandsid")).subscribe(res=>{
        console.log(res)
        this.brandsproduct=res
        this.SpinnerService.hide();
      })
    }
  
      })
   }

  ngOnInit(): void {
  }
  product(id:any){
    console.log(id)
    let navigationExtras: NavigationExtras = {
      state: {
        item: id
      }
    };
    this.router.navigate(['pages/productdetail'], navigationExtras)
  }
  addwishlist(id:any){
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
      console.log(this.cd)
      let list = {
        data:{
        "customer_id":this.cd[0].customer_id,
         "product_id":id
        }
      }
      console.log(list)
      this.crud.wishlist(list).subscribe((data:any)=> {
        console.log(data)
        if(data==="Widhlist Added"){
          this.toast.success("Widhlist Added");
        }else{
          this.toast.error("Widhlist Added Unsuccessfully");
        }
    })
  }else this.toast.error("Need login to access wishlist")
    }
    
    compare(id:any){
      this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
      if(Array.isArray(this.cd) && this.cd.length){
      console.log(this.cd)
      let com = {
        data:{
        "customer_id":this.cd[0].customer_id,
         "product_id":id
        }
      }
      this.crud.compare(com).subscribe(data => {
        console.log(data)
        this.toast.success("compare product added successfully");
    })
  }else this.toast.error("Need login to access wishlist")
    }
}
