import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CrudService } from 'src/app/services/crud.service'
import { Validators, FormBuilder, FormGroup, PatternValidator } from '@angular/forms';


@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {
  productdetails: any = [];
  productdetails1: any = []
  name: any = [];
  image: any = [];
  url: any = [];
  price: any;
  short_description: any;
  desc: any;
  sku: any;
  id: any;
  cd: any;
  quoteid: any;
  item_qty2: any = [];
  item_qty1: any = [];
  item_qty: number = 0;
  qtyform!: FormGroup;
  //items: any = [];
  minmax: any = [];
  configurableproduct: any;
  configurable: any;
  newarr: any;
  keys: any;
  typeskeys: any = [];
  colour_array: any = [];
  colour_array1: any = [];
  size_array: any = [];
  size_array1: any = [];
  ress: any = [];
  validdata: any = [];
  new_array: any = [];
  new_array1: any = [];
  res: any = [];
  hlo: any=[];
  dups: any=[];
  reviewcomment: any=[];
  rev: any=[];
  attach: any=[];
  det: any=[];
  tech: any=[];


  constructor(private crudService: CrudService, private fb: FormBuilder, private SpinnerService: NgxSpinnerService, private toast: ToastrService, public router: Router, private route: ActivatedRoute) {
    this.SpinnerService.show();
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation()?.extras.state) {
        this.productdetails1 = []
        let items = this.router.getCurrentNavigation()?.extras.state?.item;
        console.log(items)
        this.product(items);
      }
    })
    this.qtyform = this.fb.group({
      qty: [""]
    })
  }

  ngOnInit(): void {

  }

  product(items: any) {
    this.crudService.product(items).subscribe(data => {
      console.log(data)
      this.productdetails = data;
      this.productdetails1.push(this.productdetails[0]["items"])
      this.name = this.productdetails1[0].name
      this.price = this.productdetails1[0].price
      this.image = this.productdetails1[0].image
      this.short_description = this.productdetails1[0].short_description
      this.desc = this.productdetails1[0].desc
      this.url = this.productdetails1[0].url_key
      this.id = this.productdetails1[0].id
      this.sku = this.productdetails1[0].sku

      console.log(this.productdetails1)
    })
    this.crudService.minmax(items).subscribe(res => {
      console.log(res)
      this.minmax = res
      this.item_qty2 = 0;
      this.item_qty1 = 0;
      this.item_qty2 += parseFloat(this.minmax[0].min_sale_qty)
      this.item_qty1 += parseFloat(this.minmax[0].max_sale_qty)
      this.item_qty += this.item_qty2;
      if (this.item_qty2 > 0) {
        this.qtyform.get(["qty"])?.setValue(this.item_qty2)
      } else {
        this.item_qty2 = 5
        this.item_qty = 0
        this.item_qty1 = 10
        this.qtyform.get(["qty"])?.setValue(this.item_qty2)
      }
    })
    this.crudService.reviewcomment(items).subscribe(res=>{
      console.log("view",res)
      this.reviewcomment=res
    })

    this.configurableproduct = true
    //let ids = 3490
    this.crudService.configurableproduct(items).subscribe(res => {
      console.log("oii", res)
      this.hlo=res
      if (res != "") {
       if (this.hlo[0].items.config_options != undefined) {
          this.configurable = this.hlo[0].items.config_options
        // console.log(this.configurable)
         // var dups: any=[];
          this.newarr = this.configurable.filter( (el: any) => {
            if (this.dups.indexOf(el.optins_values.options_id) == -1) {
              this.dups.push(el.optins_values.options_id);
              return true;
            }
            return false;
          });
          console.log(this.dups)
          this.keys = [];
          console.log(this.newarr, "conf")
          for (let j = 0; j < this.newarr.length; j++) {
            this.keys.push(this.newarr[j].option_label)
          }
            console.log(this.keys)
          let key = this.keys;
          this.typeskeys = [];
          this.colour_array = [];
          this.colour_array1 = [];
          this.size_array = [];
          this.size_array1 = [];
          this.typeskeys = key.filter((item: any, i: any, ar: any) => ar.indexOf(item) === i);
          console.log(this.typeskeys)
          if (Array.isArray(this.newarr) && this.newarr.length) {
            this.configurableproduct = false

            for (let i = 0; i < this.newarr.length; i++) {

              if (this.typeskeys.length == 2) {
                if (this.newarr[i].option_label == 'Colour') {
                  this.colour_array.push(this.newarr[i])
                }
                else {
                  this.size_array.push(this.newarr[i])
                }
              }
              else if (this.typeskeys.length == 1) {
                this.size_array1.push(this.newarr[i])
              }
            }
            let data = this.colour_array
            this.colour_array1 = data.filter((item: any, i: any, ar: any) => ar.indexOf(item) === i);
            console.log(data)
            console.log(this.colour_array1)
            console.log(this.colour_array)
            console.log(this.size_array)
          } else {
            this.configurableproduct = true
          }
        }
        console.log(this.size_array1)
      }
    })
    this.SpinnerService.hide();
  }
  Reviews(value:any){
    alert("oi")
    this.rev=true
    this.attach=false
    this.det=false
    this.tech=false
    }
    Reviews1(value:any){
      this.rev=false
      this.attach=false
      this.det=false
      this.tech=false
      }
  img_click(colour: any) {
    
    this.validdata = [];
    this.new_array = [];
    this.new_array1 = [];
    for (let i = 0; i < this.size_array.length; i++) {

      if ((this.size_array[i].optins_values["sku"].slice(0, 6)).match((colour.optins_values.sku).slice(0, 6))) {
        this.new_array.push(this.size_array[i])
      }
    }
    console.log(this.new_array)
    this.productdetails1[0].price = colour.optins_values.price
    this.productdetails1[0].sku = colour.optins_values.sku
    this.productdetails1[0].image = colour.optins_values.image

  }
  size_click(size:any) {
    console.log(size)
    this.productdetails1[0].price = size.optins_values.price
    this.productdetails1[0].sku = size.optins_values.sku
    this.productdetails1[0].image = size.optins_values.image
    this.validdata = []
    this.validdata.push(size);
    console.log("new",this.productdetails1)
  }
  changing(value: any) {
    console.log(value)
    if (value.qty == this.item_qty2) {
      this.toast.success("you reach minimum qty")
    }
    if (value.qty < this.item_qty2) {
      this.qtyform.get(["qty"])?.setValue(this.item_qty2)
    }
    if (value.qty > this.item_qty1) {
      this.qtyform.get(["qty"])?.setValue(this.item_qty1)
      this.toast.success("you reach maximum qty")
    }
  }
  incrementQty() {
    this.item_qty = this.qtyform.get(['qty'])?.value
    this.item_qty += 1;
    this.qtyform.get(["qty"])?.setValue(this.item_qty)
    console.log(this.item_qty);
    if (this.item_qty > this.item_qty1) {
      this.qtyform.get(["qty"])?.setValue(this.item_qty1)
    }
  }
  decrementQty() {
    this.item_qty = this.qtyform.get(['qty'])?.value
    if (this.item_qty - 1 < this.item_qty2) {
      this.qtyform.get(["qty"])?.setValue(this.item_qty2)
    }
    else {
      this.item_qty -= 1;
      console.log('item_2->' + this.item_qty);
      this.qtyform.get(["qty"])?.setValue(this.item_qty)
      console.log(this.item_qty);
    }
  }
 
  addwishlist(id: any) {
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if (Array.isArray(this.cd) && this.cd.length) {
      console.log(this.cd)
      let list = {
        data: {
          "customer_id": this.cd[0].customer_id,
          "product_id": id
        }
      }
      console.log(list)
      this.crudService.wishlist(list).subscribe((data: any) => {
        console.log(data)
        if (data === "Widhlist Added") {
          this.toast.success("Widhlist Added");
        } else {
          this.toast.error("Widhlist Added Unsuccessfully");
        }
      })
    } else this.toast.error("Need login to access wishlist")
  }

  compare(id: any) {
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if (Array.isArray(this.cd) && this.cd.length) {
      console.log(this.cd)
      let com = {
        data: {
          "customer_id": this.cd[0].customer_id,
          "product_id": id
        }
      }
      this.crudService.compare(com).subscribe(data => {
        console.log(data)
        this.toast.success("compare product added successfully");
      })
    } else this.toast.error("Need login to access wishlist")
  }

  addtocart(item_qty: any, sku: any) {
    this.quoteid = JSON.parse(localStorage.getItem('quote_id') || '{}')
    console.log(this.quoteid)
    let cart = {
      cartItem: {
        "sku": sku,
        "qty": this.item_qty,
        "quote_id": this.quoteid
      }
    }
    this.crudService.addtocart(cart).subscribe(res => {
      console.log(res)
      if (res != "") {
        this.toast.success("cart added successfully")
      }
    })
  }

}
