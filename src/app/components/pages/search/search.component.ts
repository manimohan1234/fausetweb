import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CrudService } from 'src/app/services/crud.service';
import { Product } from '../../../product.model';
import { AppState } from '../../../app.state';
import { Store } from '@ngrx/store';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  products: any=[];

  constructor(private crudService: CrudService, private SpinnerService: NgxSpinnerService, private toast: ToastrService, public router: Router, private route: ActivatedRoute,private store: Store<AppState>) {
    this.SpinnerService.show();
    this.products = this.store.select(state => state.product);
    this.SpinnerService.hide();

  }

  ngOnInit(): void {
   
  }

  productss(id:any){
console.log(id)
  }
  addwishlist(id:any){

  }
  compare(id:any){

  }
}
