/** Angular Imports */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

/** Custom Routing Module */
import { PageRoutingModule } from './page-routing.module';

/** Custom Module */
import { ShellModule } from '../shell/shell.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgImageSliderModule } from 'ng-image-slider';
import { RatingModule } from 'ng-starrating';
import { ProductComponent } from './product/product.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ArrivalProductsComponent } from './arrival-products/arrival-products.component';
import { ExclusiveProductsComponent } from './exclusive-products/exclusive-products.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { MycartComponent } from './mycart/mycart.component';
import { MyaccountComponent } from './myaccount/myaccount.component';
import { BulkorderComponent } from './bulkorder/bulkorder.component';
import { ProductCompareComponent } from './product-compare/product-compare.component';
import { BrandListComponent } from './brand-list/brand-list.component';
import { QuickaddComponent } from './quickadd/quickadd.component';
import { StorelistComponent } from './storelist/storelist.component';
import { QuotelistComponent,DialogData } from './quotelist/quotelist.component';
import { QuoteconversationComponent } from './quoteconversation/quoteconversation.component';
import { MatDialogModule } from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MyorderComponent } from './myorder/myorder.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    DashboardComponent,
    ProductComponent,
    ProductDetailComponent,
    ArrivalProductsComponent,
    ExclusiveProductsComponent,
    WishlistComponent,
    MycartComponent,
    MyaccountComponent,
    BulkorderComponent,
    ProductCompareComponent,
    BrandListComponent,
    QuickaddComponent,
    StorelistComponent,
    QuotelistComponent,
    QuoteconversationComponent,
    DialogData,
    MyorderComponent,
    SearchComponent
  ],
  imports: [
    CommonModule,
    PageRoutingModule,
    ShellModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    NgImageSliderModule,
    RatingModule,
    Ng2SearchPipeModule,
    MatDialogModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
  ]
})
export class PageModule { }
