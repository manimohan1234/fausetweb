import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-myaccount',
  templateUrl: './myaccount.component.html',
  styleUrls: ['./myaccount.component.scss']
})
export class MyaccountComponent implements OnInit {
  cd: any=[];
  email: any=[];
  Logo: any=[];

  constructor(public crud:CrudService) { }

  ngOnInit(): void {
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    this.crud.customerprofile(`${this.cd[0].email}`).subscribe(res=>{
      this.email= res;
      console.log(this.email)
      let mail=this.email[0].email;
      let letter= mail.charAt(0);
      this.Logo =letter.toUpperCase();  
      localStorage.setItem('iniitial_logo',this.Logo)
    })
  }

}
