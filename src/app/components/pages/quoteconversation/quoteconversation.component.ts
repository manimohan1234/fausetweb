import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { CrudService } from 'src/app/services/crud.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quoteconversation',
  templateUrl: './quoteconversation.component.html',
  styleUrls: ['./quoteconversation.component.scss']
})
export class QuoteconversationComponent implements OnInit {
  cd: any;
  conversations: any=[];
  nodata: any;

  constructor(private crudService: CrudService,private toast: ToastrService,public router: Router) { }

  ngOnInit(): void {
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    let customerid=this.cd[0].customer_id
  //  let ids=5
     this.crudService.quotecon(customerid).subscribe(res=>{
       console.log(res)
       this.conversations=res;
       if(this.conversations.length==0){
         this.nodata=true
       }else{
        this.nodata=false
       }
     },(error:HttpErrorResponse)=>{
       if(error.status===401){          
         localStorage.setItem("reloginpath","/quoteconversation")
         this.toast.error("Session timeout please login continue");
         this.router.navigate(["/login"]);
      }
   
     })
  }

}
