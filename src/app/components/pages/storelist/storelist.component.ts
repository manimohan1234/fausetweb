import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { CrudService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-storelist',
  templateUrl: './storelist.component.html',
  styleUrls: ['./storelist.component.scss']
})
export class StorelistComponent implements OnInit {
  source: any;
  sourcelists: any=[];
  cd: any;
  quoteid: any;

  constructor(public crud: CrudService, private SpinnerService: NgxSpinnerService, private toast: ToastrService,private router:Router) { }

  ngOnInit(): void {
    this.crud.getsourcelist().subscribe(res=>{
      console.log(res)
      this.source = res;    
    })
  }
  select(val:any){
    this.SpinnerService.show();
    this.crud.showsourcelist(val).subscribe(res=>{
      console.log(res)
      this.sourcelists = res;
      this.SpinnerService.hide();
    })
  }

  addwishlist(id:any){
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
      console.log(this.cd)
      let list = {
        data:{
        "customer_id":this.cd[0].customer_id,
         "product_id":id
        }
      }
      console.log(list)
      this.crud.wishlist(list).subscribe((data:any)=> {
        console.log(data)
        if(data==="Widhlist Added"){
          this.toast.success("Widhlist Added");
        }else{
          this.toast.error("Widhlist Added Unsuccessfully");
        }
    })
  }else this.toast.error("Need login to access wishlist")
  }
  compare(id:any){
    this.cd = JSON.parse(localStorage.getItem('customer') || '{}')
    if(Array.isArray(this.cd) && this.cd.length){
    console.log(this.cd)
    let com = {
      data:{
      "customer_id":this.cd[0].customer_id,
       "product_id":id
      }
    }
    this.crud.compare(com).subscribe(data => {
      console.log(data)
      this.toast.success("compare product added successfully");
  })
}else this.toast.error("Need login to access wishlist")
  }

  addtocart(sku:any){
       this.quoteid = JSON.parse(localStorage.getItem('quote_id') || '{}')
    console.log(this.quoteid)
      let cartItem1={ 
        cartItem: {
         "sku": sku,
         "qty": "1",
         "quote_id": this.quoteid
      }
     }
      
    this.crud.addtocart(cartItem1).subscribe(res=>{
      console.log(res)
      if(res !=""){
        this.toast.success("cart added successfully")
      }
    })
     }
     product(id: any) {
      console.log(id)
      let navigationExtras: NavigationExtras = {
        state: {
          item: id
        }
      };
      this.router.navigate(['pages/productdetail'], navigationExtras)
  
    }
     
}
