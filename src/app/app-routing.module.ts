
/** Angular Imports */
import { NgModule } from '@angular/core';
import { Routes, RouterModule ,NoPreloading} from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';


const routes: Routes = [
  { path: "", redirectTo: "/login", pathMatch: "full" },

  { path: "login", component:LoginComponent },
  { path: "register", component:RegisterComponent },

  {
    path: "pages",
    loadChildren: () =>
      import("./components/pages/page.module").then(
        c => c.PageModule
      )
  },
];

/**
 * App Routing Module */
@NgModule({
  imports: [RouterModule.forRoot(routes,{ preloadingStrategy: NoPreloading, onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
