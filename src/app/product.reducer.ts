import { Product } from '../../src/app/product.model';
import { Action } from '@ngrx/store';

export const ADD_PRODUCT = 'ADD_PRODUCT';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export function addProductReducer(state: Product[] = [], action:any) {
  switch (action.type) {
    case ADD_PRODUCT:
        return [...state, action.payload];
        case REMOVE_PRODUCT:
            let newState = [...state]; 
            newState.splice(action.payload, 1);
            return newState;
    default:
        return state;
    }
}