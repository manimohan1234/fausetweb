import { Product } from '../app/product.model';

export interface AppState {
  readonly product: Product[];
}