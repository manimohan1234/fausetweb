import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService} from '../services/authentication/authentication.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm!:FormGroup;
  logindetails: any=[];
  quote_id: any=[];
  constructor(public lb:FormBuilder,public router:Router,private service: AuthenticationService) { }

  ngOnInit(): void {
    this.loginForm = this.lb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  loginSubmit(){
    let dataof = {
      data:{
        "username":this.loginForm.value.username,
        "password":this.loginForm.value.password
      }
    }
    console.log(dataof)
    this.service.login(dataof).subscribe(data => {
      console.log(data)
      this.logindetails=data
      localStorage.setItem('customer',JSON.stringify(this.logindetails));
      localStorage.setItem("JWTokens",this.logindetails[0].token)
      this.service.quote_id(data).subscribe(res => {
        console.log(res)
        this.quote_id = res
        localStorage.setItem('quote_id',JSON.stringify(this.quote_id));
      })
      console.log()
      this.router.navigate(['/pages/dashboard'])
    },err=>{
      
    })

  }
  
}