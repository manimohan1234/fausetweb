import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication/authentication.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form!: FormGroup;
  constructor(public lb: FormBuilder, public router: Router, private service: AuthenticationService) { }

  ngOnInit(): void {
    this.form = this.lb.group({
      email: ['', Validators.required],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      password: ['', Validators.required]
    })
  }
  register() {
    let dataof = {
      data:{
        "email":this.form.value.email,
        "firstname":this.form.value.firstname,
        "lastname":this.form.value.lastname,
        "password":this.form.value.password
      }
    }
    console.log(dataof)
    this.service.reg(dataof).subscribe(data => {
      console.log(data)
      alert("Your Account Created")
      this.router.navigate([''])
    },err=>{
      
    })
    
  }
  }

